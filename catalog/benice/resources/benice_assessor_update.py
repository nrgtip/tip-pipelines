#!/usr/bin/env python

# from lxml.builder import ElementMaker
from lxml import etree
import subprocess
import os, sys, argparse

versionNumber='1'
dateString='20150618'
author='flavin'
progName=sys.argv[0].split('/')[-1]
idstring = '$Id: %s,v %s %s %s Exp $'%(progName,versionNumber,dateString,author)

print idstring

#######################################################
# PARSE INPUT ARGS
parser = argparse.ArgumentParser(description='Generate XML assessor file from PUP output logs')
parser.add_argument('-v', '--version',
                    help='Print version number and exit',
                    action='version',
                    version=versionNumber)
parser.add_argument('--idstring',
                    help='Print id string and exit',
                    action='version',
                    version=idstring)
parser.add_argument('-s','--setupScript',
                    help='Path to Benice setup script.')
parser.add_argument('-p','--paramsXML',
                    help='Path to pipeline input params XML file.')
parser.add_argument('-r','--repoCSV',
                    help='Benice repo information: host,owner,name')
parser.add_argument('-i','--assessorIn',
                    help='Path to existing assessor XML file (input)')
parser.add_argument('-o','--assessorOut',
                    help='Path to output assessor XML file (if not specified, will overwrite input file)')
parser.add_argument('--debug',action='store_true')
args=parser.parse_args()

assessorIn = args.assessorIn
if args.assessorOut:
    assessorOut = args.assessorOut
else:
    assessorOut = assessorIn

if args.repoCSV:
    repoInfo = args.repoCSV.split(',')
    if len(repoInfo)!=3:
        sys.exit("Improper input argument repoCSV: "+args.repoCSV)
#######################################################


# Get the repo hash
if args.setupScript:
    with open(args.setupScript) as f:
        for line in f.readlines():
            if 'TOOLS_DIR=' in line:
                toolsDir = line.strip().split('=')[1].strip('"')

    if toolsDir:
        cwd = os.getcwd()
        os.chdir(toolsDir)
        try:
            repoHash = subprocess.check_output('hg --debug id -i',shell=True).strip()
        except:
            repoHash = ""
        os.chdir(cwd)

# Read in existing assessor
print 'Reading assessor XML from %s'%assessorIn
assessorXML = etree.parse(assessorIn)
assessorRoot = assessorXML.getroot()
tipns = assessorRoot.nsmap['tip']

# Read in params XML
paramsXML = etree.parse(args.paramsXML)
paramsRoot = paramsXML.getroot()

#######################################################
# WRITE ADDITIONAL STUFF INTO ASSESSOR
inputs = etree.SubElement(assessorRoot, "{%s}inputs"%tipns)

if args.debug: print ""
# Copy input params from pipeline params file to assessor
paramsOut = etree.SubElement(inputs, "{%s}params"%tipns)
for paramIn in paramsRoot.getchildren():
    if args.debug: print "Input param"

    nameInEl = paramIn.xpath('pip:name',namespaces=paramsRoot.nsmap)
    if len(nameInEl)==0:
        if args.debug: print "no name"
        name = ""
    elif len(nameInEl)==1:
        name = nameInEl[0].text
        if args.debug: print name
    else:
        print "An input parameter has more than one name. How did that even happen?"
        for nameEl in nameInEl:
            print "Name: "+nameEl.text
        sys.exit("Exiting now.")

    valueInEl = paramIn.xpath('pip:values/pip:unique',namespaces=paramsRoot.nsmap)
    if len(valueInEl)==0:
        valueInList = paramIn.xpath('pip:values/pip:list',namespaces=paramsRoot.nsmap)
        if len(valueInList)==0:
            if args.debug: print "no value"
            value = ""
        else:
            value = "[{}]".format(",".join(val.text for val in valueInList))
            if args.debug: print value
    elif len(valueInEl)==1:
        value = valueInEl[0].text
        if args.debug: print value
    else:
        print "An input parameter has more than one unique value. How did that even happen?"
        for valueEl in valueInEl:
            print "Value: "+valueEl.text
        sys.exit("Exiting now.")

    if name is not "" or value is not "":
        if args.debug: print "Adding to benice assessor"
        paramOut = etree.SubElement(paramsOut, "{%s}param"%tipns)
        etree.SubElement(paramOut, "{%s}name"%tipns).text = name
        etree.SubElement(paramOut, "{%s}value"%tipns).text = value

    if args.debug: print ""

# Write Benice repo information
if args.repoCSV:
    repo = etree.SubElement(inputs, "{%s}repo"%tipns)
    etree.SubElement(repo, "{%s}host"%tipns).text = repoInfo[0]
    etree.SubElement(repo, "{%s}owner"%tipns).text = repoInfo[1]
    etree.SubElement(repo, "{%s}name"%tipns).text = repoInfo[2]
    if repoHash:
        etree.SubElement(repo, "{%s}commitHash"%tipns).text = repoHash

#######################################################
# Spit out finished assessor
print 'Writing assessor XML to %s'%assessorOut
# print(xmltostring(assessorXML, pretty_print=True))
with open(assessorOut,'w') as f:
    f.write(etree.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True))

