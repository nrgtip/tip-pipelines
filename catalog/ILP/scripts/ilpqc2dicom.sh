#!/bin/bash

if [ "$1" == "--version" ]; then
    echo 1
    exit 0
fi

die(){
    echo >&2 "$@"
    exit 1
}

# Input args
seriesBase=$1
dicomInDir=$2
dicomOutDirBase=$3

qcImageList=(`ls`)

for i in ${!qcImageList[*]}; do
    if (($i < 10 )); then
        dirName="${seriesBase}00$i"
    elif (($i < 100)); then
        dirName="${seriesBase}0$i"
    elif (($i < 1000)); then
        dirName="${seriesBase}$i"
    else
        die "All numbers under 1000 already used for file names in target directory"
    fi

    seriesDesc1=${qcImageList[$i]#*_}
    seriesDesc2=${seriesDesc1%%.*}
    seriesDesc="ILP_"$seriesDesc2

    set -x
    mkdir $dirName || die "Could not make directory $dirName"
    cp ${qcImageList[$i]} $dirName || die "Could not copy $qcImageList[$i] to $dirName"

    /data/tip/pipeline/catalog/ILP/bin/analyze2dcm -g c -n $dirName -o $dicomOutDirBase/$dirName -d $seriesDesc $dicomInDir $dirName || die "Error wrapping ${qcImageList[$i]} in DICOM"
    set +x
done
