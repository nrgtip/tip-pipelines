#!/bin/env python
import sys, json, argparse, requests, datetime, csv, os
from xml.etree import cElementTree as ElementTree
from dateutil.relativedelta import relativedelta

# This script takes a scan's accession number and returns two csvs, one containing all the patient's sessions and another containing their ages at the time of each session.


# parse arguments to the script
parser = argparse.ArgumentParser(description='Create a list of sessions and ages to be used by FS_stats_maker.sh.')
parser.add_argument('mrId',help="MR session id")
parser.add_argument('host',help="TIP host")
parser.add_argument('user',help="TIP username")
parser.add_argument('pwd',help="TIP password")
args=parser.parse_args()


# take in sessionId - this will be used to find the subject ID
sessionId = args.mrId
host = args.host
user = args.user
pwd = args.pwd


print("session id: " + sessionId)

# set namespace for ElementTree
namespaces = {'xnat': 'http://nrg.wustl.edu/xnat'}

# get subjectID out of xml result
r = requests.get(host + 'data/projects/ILP/experiments/' + sessionId + '?format=xml', auth=(user, pwd), verify=False)
root = ElementTree.fromstring(r.text)
subjectId = root.find('xnat:subject_ID', namespaces).text

# obtain sessions and session dates using subjectId
# create list of session numbers
r2 = requests.get(host + 'data/subjects/' + subjectId + '?format=xml', auth=(user, pwd), verify=False)
root = ElementTree.fromstring(r2.text)

session_list = []
session_dates_list = []
age_list = []
subject_birthdate = None

qc_passed = 0
has_assessors = 0

for child in root[0]:

	# pull stats files for past sessions and put them in a folder
	# want [session number]/stats/aseg.stats, [session number]/stats/lh.aparc.stats, [session number]/stats/rh.aparc.stats
	# first check that the assessor exists - this session may not have a Freesurfer on it
	assessor_list = child.find('xnat:assessors', namespaces)
	if assessor_list is not None:
		# check multiple assessors in the list
		for assessor in assessor_list:
			# mark that at least one assessor exists
			has_assessors = 1
			# check validation status and omit it if the freesurfer failed
			qc_status = assessor.find('xnat:validation', namespaces)
			
			if (qc_status is not None) and (qc_status.attrib['status'] != "Failed") and (qc_status.attrib['status'] != "Failed with edits") and (qc_status.attrib['status'] != "Failed-needs reprocessing") and (qc_status.attrib['status'] != "Quarantined") and (qc_status.attrib['status'] != ""):
				print("assessor: " + assessor.attrib['ID'])
				print(qc_status.attrib['status'])
				print("passed")
				# mark that at least one assessor has passed QC
				qc_passed = 1
				# only get this info if there is an assessor and the qc exists and hasn't failed
				# accession number for session
				session_number = child.attrib['label']
				if session_number not in session_list:
					session_list.append(session_number)
				# date of session
				session_date = datetime.datetime.strptime(child.find('xnat:date', namespaces).text, "%Y-%m-%d")
				session_dates_list.append(session_date)

				# get subject birthdate just once
				if subject_birthdate is None:
					subject_birthdate = datetime.datetime.strptime(child.find('xnat:dcmPatientBirthDate', namespaces).text, "%Y-%m-%d")

				# calculate age at each session date based on session date and birthdate
				# only do this if there is an assessor for the freesurfer
				age_at_session = relativedelta(session_date, subject_birthdate).years
				age_list.append(age_at_session)
				
				if not os.path.isdir(session_number + '/stats'):
					os.makedirs(session_number + '/stats')
					# only create stats files if you just made the directories
					# get assessor ID, mrLabel from xml file to use in REST call
					#assessor_list = child.find('xnat:assessors', namespaces)
					assessorId = assessor.attrib['ID']
					mrLabel = child.attrib['label']

					# construct the url to grab files from REST call
					url = host + 'data/experiments/' + sessionId + '/assessors/' + assessorId + '/resources/DATA/files/' + mrLabel + '/stats/'
					# get aseg.stats: write resulting files to new file in directory
					with open(os.path.join(session_number + '/stats', 'aseg.stats'), "w") as file1:
						toFile = requests.get(url + 'aseg.stats', auth=(user, pwd), verify=False)
						file1.write(toFile.text)
					# get lh.aparc.stats: write resulting files to new file in directory
					with open(os.path.join(session_number + '/stats', 'lh.aparc.stats'), "w") as file2:
						toFile = requests.get(url + 'lh.aparc.stats', auth=(user, pwd), verify=False)
						file2.write(toFile.text)
					# get rh.aparc.stats: write resulting files to new file in directory
					with open(os.path.join(session_number + '/stats', 'rh.aparc.stats'), "w") as file3:
						toFile = requests.get(url + 'rh.aparc.stats', auth=(user, pwd), verify=False)
						file3.write(toFile.text)
# raise errors if there are no assessors or if qc did not pass for any of them
if has_assessors == 0:
	raise ValueError("No FreeSurfers were found to run ILP on. Run a FreeSurfer and try again.")
if qc_passed == 0:
	raise ValueError("No FreeSurfers passed QC. Mark at least one FreeSurfer as passed and try again.")

# write ages to csv file for use in FS_stats_maker
with open(sessionId + '_age_list.csv', 'wb') as fp:
    a = csv.writer(fp, delimiter='\t')
    a.writerow(["Age"])
    data = age_list
    for i in data:
    	a.writerow([i])

# write session list to csv file for use in FS_stats_maker
with open(sessionId + '_session_list.csv', 'wb') as fp:
    a = csv.writer(fp, delimiter=',')
    #a.writerow(["Session"]) # don't need this - freesurfer needs session #'s only
    data = session_list
    for i in data:
    	a.writerow([i])
