#!/bin/bash

NIL_TOOLS_HOME=/nrgpackages/tools.release/lin64-nil-tools-20140730/
NILSRC=/nrgpackages/tools.release/lin64-nilsrc-20140730/
RELEASE=${NIL_TOOLS_HOME}
REFDIR=/nrgpackages/atlas
TRX=$NILSRC/TRX
PATH=${NIL_TOOLS_HOME}:${TRX}:${PATH}
export NIL_TOOLS_HOME NILSRC RELEASE REFDIR TRX PATH
